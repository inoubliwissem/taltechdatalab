#!/usr/bin/env bash
#Usage: hadoop-install.sh workdir
#we can make the WorkDir as in input

set -e

function do_check {
  if [[ ! -d $WORKDIR ]]; then
    echo -ne "\n\n$WORKDIR does not exist ..."
    echo -ne "\n\truning: mkdir -p $WORKDIR"
    mkdir -p $WORKDIR
  fi
  if [[ ! -d $HADOOP_HOME ]]; then
    echo -ne "\n\nhadoop home does not exist ..."
    echo -ne "\n\truning: mkdir -p $HADOOP_HOME\n"
    mkdir -p $HADOOP_HOME
  fi
}

function do_download {
  wget --quiet --show-progress --progress=bar:mega $HADOOP_URL -O $TMP_DIR/$HADOOP_ARCHIVE
  echo -ne "\n\nhadoop archive located at \n\t$TMP_DIR/$HADOOP_ARCHIVE"
}

function do_extract {
  echo -ne "\n\nextracting hadoop archive to \n\t$HADOOP_HOME\n"
  tar -xzf  $TMP_DIR/$HADOOP_ARCHIVE -C $TMP_DIR
  echo -ne "\n\ncopy files to \n\t$HADOOP_HOME\n"
  mv $(echo $TMP_DIR/hadoop-$HADOOP_VERSION/*) $HADOOP_HOME
}

function do_configure {
  echo -ne "\n\nremove unecessary files (windows files)"
  echo -ne "\n\trm -f $HADOOP_HOME/bin/*.cmd"
  rm -f $HADOOP_HOME/bin/*.cmd
  echo -ne "\n\trm -f $HADOOP_HOME/sbin/*.cmd\n"
  rm -f $HADOOP_HOME/sbin/*.cmd
  echo -ne "\n\tmv --verbose $PWD/.hadooprc $HOME/.hadooprc\n"
  mv --verbose $PWD/.hadooprc $HOME/
  export_conf $HOME/.hadooprc
  echo -ne "\n\noverride hadoop config"
  echo -ne "\n\tcp -fv ../etc/hadoop/* $HADOOP_HOME/etc/hadoop\n"
  cp -fv $HADOOP_CONF_DIR/* $HADOOP_HOME/etc/hadoop
}


function do_configure_slaves {
  SLAVES=$(cat $HADOOP_CONF_DIR/slaves)
  for SLAVE in $SLAVES; do
    ssh $HADOOP_USER@$SLAVE "mkdir -p $HADOOP_HOME && exit"
    rsync -av --progress $HADOOP_HOME/ $HADOOP_USER@$SLAVE:$HADOOP_HOME
    echo -ne "\n\ncopy configuration files to $SLAVE\n"
    scp -p $HADOOP_CONF_DIR/* $HADOOP_USER@$SLAVE:$HADOOP_HOME/etc/hadoop
    scp -p $HOME/.hadooprc $HADOOP_USER@$SLAVE:~
  done
}

function export_conf {
  echo "if [[ -f $1 ]]; then
    source $1
  fi" >> $HOME/.bashrc
  source $HOME/.bashrc
}

function export_var {
  echo "export $1=$2" >> ~/.bashrc
}

function usage {
  case $1 in
    workdir )
      echo -ne "Usage: `basename $0` <workdir>\n"
      ;;
  esac
}

if [[ $1 == "" ]]; then
  echo -ne "\n\nset workdir to $HOME/lab/solution (default)\n"
  WORKDIR=$HOME/lab/solution
else
  WORKDIR=$1
fi

HADOOP_URL="https://www-eu.apache.org/dist/hadoop/common/hadoop-2.9.2/hadoop-2.9.2.tar.gz"
HADOOP_ARCHIVE="hadoop-2.9.2.tar.gz"
HADOOP_VERSION=2.9.2
HADOOP_HOME=$WORKDIR/hadoop/$HADOOP_VERSION
HADOOP_CONF_DIR=$(echo $PWD/../../etc/hadoop)
TMP_DIR=/tmp
HADOOP_USER=$CLUSTER_USER
HADOOP_GROUP=hadoop

do_check
do_download
do_extract
do_configure
do_configure_group
do_configure_slaves

export_var HADOOP_VERSION $HADOOP_VERSION
export_var HADOOP_HOME $WORKDIR/hadoop/$HADOOP_VERSION
export_var HADOOP_CONF_DIR $HADOOP_HOME/etc/hadoop
export_var PATH $PATH:$HADOOP_HOME/bin
export_var PATH $PATH:$HADOOP_HOME/sbin

echo -ne "\n\nTodo:\n"
echo -ne "\trun: source ~/.bashrc\n"
