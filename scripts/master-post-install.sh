#!/usr/bin/env bash

function copy_keys() {
  echo -ne "******* Copy keys to slaves *******"
  for SLAVE in $SLAVES; do
    echo -ne "copy keys from $MASTER to $SLAVE"
    ssh-copy-id $HADOOP_GENERIC_USER@$SLAVE
  done
}

function password_less() {
  echo -ne "enable a passwordless connection between host and slaves\n"
  for SLAVE in $SLAVES; do
    echo -ne "ssh $HADOOP_GENERIC_USER@$SLAVE mkdir -p ~/.ssh\n"
    ssh $HADOOP_GENERIC_USER@$SLAVE mkdir -p ~/.ssh
    cat .ssh/id_rsa.pub | ssh $HADOOP_GENERIC_USER@$SLAVE "cat >> ~/.ssh/authorizd_keys"
    echo -ne "******* set permissions *******\n"
    echo -ne "ssh hadoop@$SLAVE 'chmod 700 ~/.ssh; chmod 640 ~/.ssh/authorizd_keys'"
    ssh $HADOOP_GENERIC_USER@$SLAVE "chmod 700 ~/.ssh; chmod 640 ~/.ssh/authorizd_keys"
  done
}

function test_keys() {
  echo -ne "***** Test Keys *****"
  for SLAVE in $SLAVES; do
    ssh $HADOOP_GENERIC_USER@$SLAVE exit
    if [[ $? -eq 0 ]]; then
      echo -ne "$SLAVE works...\n"
    fi
  done
}

function install_java() {
  # TODO: run apt without sudo
  echo -ne "\n******* Installing Java (openjdk 8) ******* \n"
  sudo apt-get --yes install openjdk-8-jdk
  echo -ne "\n\t******* set JAVA_HOME *******\n"
  echo -ne "\necho export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 >> .bashrc \n"
  echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 " >> ~/.bashrc
  source ~/.bashrc
  echo -ne "\n******* update PATH *******\n"
  echo -ne "\necho export PATH=$PATH:$JAVA_HOME/bin >> .bashrc \n"
  echo "export PATH=$PATH:$JAVA_HOME/bin" >> ~/.bashrc
  echo "OPENJDK version : " java --version | head -1 | cut -d' ' -f2
}

function generate_keys() {
  echo -ne "\n********** Generating SSH Keys **********\n"
  ssh-keygen -t rsa -b 1024 -C "cluster keys" -N "" -f ~/.ssh/id_rsa 2> /dev/null
  echo -ne "\n\tpublic key located at: ~/.ssh/id_rsa.pub"
}

function send_scripts {
  for SLAVE in $SLAVES; do
    SLAVE_IP=$(get_slave_ip $SLAVE)
    echo -ne "\n***** send slave-post-install.sh -> $SLAVE ($SLAVE_IP) *****\n";
    rsync -azv ./slave-post-install.sh $HADOOP_GENERIC_USER@$SLAVE:~
  done
}

function get_slave_ip() {
  IP=$(host $1 | head -1 | cut -d' ' -f4)
  echo $IP
}

function usage {
  echo "Usage : $0 <slave1> <slave2> ... <slaveN>"
  echo "$0 -h   show this help"
}

if [[ $# -le 0 ]]; then
  usage
  exit 1
fi

if [[ $1 == "-h" ]]; then
  usage
  exit 1
fi

HADOOP_GENERIC_USER=`whoami`
MASTER=$(hostname)
SLAVES=$@

generate_keys
copy_keys
test_keys
password_less
install_java
send_scripts

echo -ne "\nexport CLUSTER_USER=$HADOOP_GENERIC_USER\n"
echo "export CLUSTER_USER=$HADOOP_GENERIC_USER" >> ~/.bashrc