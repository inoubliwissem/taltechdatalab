#!/usr/bin/env bash
#usage: bash -C spark-install.sh workdir

set -e
set +x

function do_check {
  if [[ ! -d $WORKDIR ]]; then
    echo -ne "\n\n$WORKDIR does not exist ..."
    echo -ne "\n\truning: mkdir -p $WORKDIR"
    mkdir -p $WORKDIR
  fi
  if [[ ! -d $SPARK_HOME ]]; then
    echo -ne "\n\nspark home does not exist ..."
    echo -ne "\n\truning: mkdir -p $SPARK_HOME\n"
    mkdir -p $SPARK_HOME
  fi
}

function do_download {
  wget --quiet --show-progres $SPARK_URL -O $TMP_DIR/$SPARK_ARCHIVE
  echo -ne "\n\nspark archive located at \n\t$TMP_DIR/$SPARK_ARCHIVE"
}

function do_extract {
  echo -ne "\n\nextract spark archive:\n"
  echo -ne "\n\nextracting spark archive to \n\t$SPARK_HOME\n"
  tar -xzf  $TMP_DIR/$SPARK_ARCHIVE -C $TMP_DIR
  echo -ne "\n\ncopy files to \n\t$SPARK_HOME\n"
  mv $(echo $TMP_DIR/spark-$SPARK_VERSION-bin-hadoop2.7/*) $SPARK_HOME
}

function do_configure {
  echo -ne "\n\nremove unecessary files (windows files)"
  echo -ne "\n\trm -f $SPARK_HOME/bin/*.cmd"
  rm -f $SPARK_HOME/bin/*.cmd
  echo -ne "\n\trm -f $SPARK_HOME/sbin/*.cmd\n"
  rm -f $SPARK_HOME/sbin/*.cmd
  echo -ne "\n\tmv --verbose $PWD/.sparkrc $HOME/.sparkrc\n"
  mv --verbose $PWD/.sparkrc $HOME/
  export_conf $HOME/.sparkrc
  echo -ne "\n\noverride hadoop config"
  echo -ne "\n\tcp -fv ../etc/spark/* $SPARK_HOME/conf/\n"
  cp -fv $SPARK_CONF_DIR/* $SPARK_HOME/conf/
}

function do_configure_slaves {
    SLAVES=$(cat $SPARK_CONF_DIR/slaves)
    for SLAVE in $SLAVES; do
        ssh $SPARK_USER@$SLAVE "mkdir -p $SPARK_HOME && exit"
        rsync -av $SPARK_HOME/ $SPARK_USER@$SLAVE:$SPARK_HOME
        echo -ne "\n\ncopy .sparkc to $SLAVE\n"
        rsync -av $HOME/.sparkrc $SPARK_USER@$SLAVE:~
    done
}

function export_conf {
  echo "if [[ -f $1 ]]; then
    source $1
  fi" >> $HOME/.bashrc
  source $HOME/.bashrc
}

function usage {
  echo -ne "\nusage: bash -C spark-install.sh <workdir>\n"
  exit 1
}



if [[ -z $1 ]]; then
  usage
fi

WORKDIR=$1
SPARK_URL="https://archive.apache.org/dist/spark/spark-2.1.0/spark-2.1.0-bin-hadoop2.7.tgz"
SPARK_VERSION="2.1.0"
SPARK_HOME=$WORKDIR/spark/$SPARK_VERSION
SPARK_CONF_DIR=$(echo $PWD/../../etc/spark)
SPARK_ARCHIVE="spark-$SPARK_VERSION-bin-hadoop2.7"
TMP_DIR=/tmp
SPARK_USER=$CLUSTER_USER

do_check
do_download
do_extract
do_configure
do_configure_slaves
