#!/usr/bin/env

function install_java() {
  # TODO: run apt without sudo
  echo -ne "\n******* Installing Java (openjdk 8) ******* \n"
  sudo apt-get --yes install openjdk-8-jdk
  echo -ne "\n******* set JAVA_HOME *******\n"
  JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
  echo -ne "\n\ŧecho export JAVA_HOME=$JAVA_HOME >> .bashrc \n"
  echo "export JAVA_HOME=$JAVA_HOME" >> ~/.bashrc
  echo -ne "\n\t******* update PATH *******\n"
  echo -ne "\necho export PATH=$PATH:$JAVA_HOME/bin >> .bashrc \n"
  export PATH=$PATH:$JAVA_HOME/bin
  echo "export PATH=$PATH:$JAVA_HOME/bin" >> ~/.bashrc
  echo "OPENJDK version : " java --version | head -1 | cut -d' ' -f2
}

install_java
