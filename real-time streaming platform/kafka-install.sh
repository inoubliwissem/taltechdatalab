#!/usr/bin/env bash

#Usage: ./kafka-install.sh workers

set -e


function do_check {
    if [[ ! -d $KAFKA_HOME ]]; then
        echo -ne "\n\n$KAFKA_HOME does not exist\n"
        echo -ne "\trunning: mkdir -p $KAFKA_HOME\n"
        mkdir -p $KAFKA_HOME
    fi
}

function do_download {
  wget --quiet --show-progress $KAFKA_URL -O $TMP_DIR/$KAFKA_ARCHIVE
  echo -ne "\n\nkafka archive located at \n\t$TMP_DIR/$KAFKA_ARCHIVE"
}

function do_extract { 
  echo -ne "\n\nextract kafka archive:\n"
  echo -ne "\n\nextracting kafka archive to \n\t$KAFKA_HOME\n"
  tar -xzf  $TMP_DIR/$KAFKA_ARCHIVE -C $TMP_DIR
}

function do_send_to_workers {
    for WORKER in $WORKERS; do
        ssh $CLUSTER_USER@$WORKER "mkdir -p $KAFKA_HOME && exit"
        ssh $CLUSTER_USER@$WORKER "chmod 744 -R $KAFKA_HOME && exit"
        ssh $CLUSTER_USER@$WORKER "chown -R $CLUSTER_USER $KAFKA_HOME && exit"
        rsync -av --exclude=$TMP_DIR/kafka_2.11-$KAFKA_VERSION/config/server.properties $TMP_DIR/kafka_2.11-$KAFKA_VERSION/ $CLUSTER_USER@$WORKER:$KAFKA_HOME
    done
}


if [[ $1 == "" ]]; then
    echo -ne "\n\nUsage: `basename $0` <workdir> <workers>\n"
    exit 1
else
    WORKDIR=$1
fi

function export_var {
  echo "export $1=$2" >> $HOME/.bashrc
}

WORKERS=$(cat $2)
KAFKA_URL="https://www-us.apache.org/dist/kafka/2.3.0/kafka_2.11-2.3.0.tgz"
KAFKA_VERSION="2.3.0"
KAFKA_HOME=$WORKDIR/kafka/$KAFKA_VERSION
KAFKA_ARCHIVE="kafka_2.11-$KAFKA_VERSION.tar.gz"
KAFKA_USER=$CLUSTER_USER
TMP_DIR=/tmp

do_check
do_download
do_extract
do_send_to_workers

echo -ne "\n\n export kafka global Variables\n"
export_var KAFKA_HOME $KAFKA_HOME
export_var PATH "$PATH:$KAFKA_HOME/bin"


for WORKER in $WORKERS; do
    set +x
    rsync ~/.bashrc $CLUSTER_USER@$WORKER:~
    rsync -av $PWD/template $CLUSTER_USER@$WORKER:$TMP_DIR
    rsync -av $PWD/build-conf-kafka.py $CLUSTER_USER@$WORKER:$TMP_DIR
    ssh $CLUSTER_USER@$WORKER "$TMP_DIR/build-conf-kafka.py --template $TMP_DIR/template/kafka.json --kf-home $KAFKA_HOME"
    set -x
done

echo -ne "\n\nTodo:\n"
echo -ne "\trun: source ~/.bashrc\n"