#!/usr/bin/env python3

import argparse
import sys
import json
from os import path

parser = argparse.ArgumentParser()

parser.add_argument('--kf-home', help='kafka home directory')
parser.add_argument('--template', help='template file to use')

def generate_part(part):
    lcfg = ""
    for option in part:
        default = part[option]
        print("enter {}: (default = {})".format(option, default))
        print("press Enter to use default Value")
        val = input()
        if val == "":
            new_val = default
        else:
            new_val = val
        lcfg = lcfg + "{}={}\n".format(option, new_val)
    return lcfg

def main(args):
    kf_home = args.kf_home
    kafka_cfg = open(path.join(kf_home, 'config', 'server.properties'), 'w+')
    template = args.template
    print("Generate server.properties file")
    try:
        with open(template, 'r') as f:
            params = json.load(f)
        kafka_cfg.write(generate_part(params["server"]))
        kafka_cfg.write(generate_part(params["zookeeper"]))
        print("generate server.properties at ", path.join(kf_home, 'config'))
    except Exception as e:
        print(e.__cause__)
        sys.exit(1)


if __name__ == '__main__':
    main(parser.parse_args())
    