#!/usr/bin/env bash

#Usage: ./prepare.sh workers

set -e

function copy_keys() {
  echo -ne "\n******* Copy keys to workers *******\n"
  for WORKER in $WORKERS; do
    echo -ne "copy keys from `hostname` to $WORKER"
    ssh-copy-id $CLUSTER_USER@$WORKER
  done
}

function password_less() {
  echo -ne "\n\nenable a passwordless connection between host and slaves\n"
  for WORKER in $WORKERS; do
    echo -ne "ssh $CLUSTER_USER@$WORKER mkdir -p ~/.ssh\n"
    ssh $CLUSTER_USER@$WORKER mkdir -p ~/.ssh
    cat ~/.ssh/id_rsa.pub | ssh $CLUSTER_USER@$WORKER "cat >> ~/.ssh/authorizd_keys"
    echo -ne "******* set permissions *******\n"
    echo -ne "ssh $CLUSTER_USER@$WORKER 'chmod 700 ~/.ssh; chmod 640 ~/.ssh/authorizd_keys'"
    ssh $CLUSTER_USER@$WORKER "chmod 700 ~/.ssh; chmod 640 ~/.ssh/authorizd_keys"
  done
}

function test_keys() {
  echo -ne "***** Test Keys *****"
  for WORKER in $WORKERS; do
    ssh $CLUSTER_USER@$WORKER exit
    if [[ $? -eq 0 ]]; then
      echo -ne "$WORKER works...\n"
    fi
  done
}

function generate_keys() {
  echo -ne "\n********** Generating SSH Keys **********\n"
  ssh-keygen -t rsa -b 1024 -C "kafka cluster keys" -N "" -f ~/.ssh/id_rsa 2> /dev/null
  echo -ne "public key located at: ~/.ssh/id_rsa.pub"
}

if [[ -f $1 ]]; then 
    WORKERS=$(cat $1)
else
    echo "usage: `basename $0` workers"
    exit 2
fi

CLUSTER_USER=`whoami`

echo -ne "\n\nexport cluster user\n"
echo "export CLUSTER_USER=$CLUSTER_USER " >> ~/.bashrc

generate_keys
copy_keys
password_less
test_keys

if [[ -f "$PWD/java.sh" ]]; then
    for WORKER in $WORKERS; do
        rsync $PWD/java.sh $CLUSTER_USER@$WORKER:~
    done
fi

echo -ne "\n\n Installing Java in Every Worker \n"
for WORKER in $WORKERS; do
    ssh -t $CLUSTER_USER@$WORKER "bash ~/java.sh"
done