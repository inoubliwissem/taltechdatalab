#!/usr/bin/env bash

set -e

function do_check {
    if [[ ! -d $ZOOKEEPER_HOME ]]; then
        echo -ne "\n\n$ZOOKEEPER_HOME does not exist\n"
        echo -ne "\trunning: mkdir -p $ZOOKEEPER_HOME\n"
        mkdir -p $ZOOKEEPER_HOME
    fi
}

function do_download {
  wget --quiet --show-progress $ZOOKEEPER_URL -O $TMP_DIR/$ZOOKEEPER_ARCHIVE
  echo -ne "\n\nzookeper archive located at \n\t$TMP_DIR/$ZOOKEEPER_ARCHIVE"
}

function do_extract { 
  echo -ne "\n\nextract zoookeeper archive:\n"
  echo -ne "\n\nextracting zookeeper archive to \n\t$ZOOKEEPER_HOME\n"
  tar -xzf  $TMP_DIR/$ZOOKEEPER_ARCHIVE -C $TMP_DIR
  cp -r $(echo $TMP_DIR/apache-zookeeper-$ZOOKEEPER_VERSION-bin/*) $HADOOP_HOME
}


if [[ $1 == "" ]]; then
    echo -ne "\n\nUsage: `basename $0` <workdir>\n"
    exit 1
else
    WORKDIR=$1
fi

function export_var {
  echo "export $1=$2" >> $HOME/.bashrc
}

ZOOKEEPER_URL="https://www-us.apache.org/dist/zookeeper/stable/apache-zookeeper-3.5.5-bin.tar.gz"
ZOOKEEPER_VERSION="3.5.5"
ZOOKEEPER_HOME=$WORKDIR/zookeeper/$ZOOKEEPER_VERSION
ZOOKEEPER_ARCHIVE="apache-zookeeper-$ZOOKEEPER_VERSION-bin.tar.gz"
ZOOKEEPER_USER=$CLUSTER_USER
TMP_DIR=/tmp

do_check
do_download
do_extract



echo -ne "\n\n export zookeeper global Variables\n"
export_var ZOOKEEPER_HOME $ZOOKEEPER_HOME
export_var PATH "$PATH:$ZOOKEEPER_HOME/bin"

echo -ne "\n\nTodo:\n"
echo -ne "\trun: source ~/.bashrc\n"