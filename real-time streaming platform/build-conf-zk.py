#!/usr/bin/env python3

import argparse
import sys
import json
from os import path

parser = argparse.ArgumentParser()

parser.add_argument('--zk-home', help='zookeeper home directory')
parser.add_argument('--template', help='template file to use')

def main(args):
    zk_home = args.zk_home
    zoo_cfg = open(path.join(zk_home, 'conf', 'zoo.cfg'), 'w+')
    template = args.template
    print("Generate zoo.cfg file")
    cfg = ""
    try:
        with open(template, 'r') as f:
            params = json.load(f)
        for option, default in params.items():
            print("enter {}: (default = {})".format(option, default))
            print("press Enter to use default Value")
            val = input()
            if val == "":
                new_val = default
            else:
                new_val = val
            cfg = cfg + "{}={}\n".format(option, new_val)
        zoo_cfg.write(cfg)
        print("generate zoo.cfg at ", path.join(zk_home, 'conf'))
    except Exception as e:
        print(e.__cause__)
        sys.exit(1)


if __name__ == '__main__':
    main(parser.parse_args())
    